#!/bin/bash

for i in {15..50}; do
  CMD="./find_best_tsp.py -t inst/tsp_N${i}_1.txt -o output/N${i}"
  echo $CMD
  eval $CMD
done
