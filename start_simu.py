#!/usr/bin/env python




import sys
import random
from BChain import *
import multiprocessing
import logging
import multiprocessing.sharedctypes
import argparse


#from multiprocessing import Process, Lock
#from multiprocessing.sharedctypes import Value, Array
#from ctypes import Structure, c_double,c_wchar_p, c_char_p, c_int

def parse_args():
      parser=argparse.ArgumentParser(description='Running TSP for blockchain simulation.')
      parser.add_argument('-t','--tsp-file',required=True,help='TSP problem definition file')
      parser.add_argument('-n','--num-players',type=int,default=3,help='Number of players, default 3')
      parser.add_argument('-o','--output-prefix',help='Output prefix',default='')

      args=parser.parse_args()
      return args

def thread_p_func(minerx,shared_m,shared_arr):
    #minerx=miner_pool[i]
    minerx.pseudo_mining(shared_m,shared_arr)

def setup_logging(n,args):
    logmode="w"
    logging.basicConfig(level=10,
      format='%(levelname)-5s @ %(asctime)s: %(message)s ',
      datefmt='%a, %d %b %Y %H:%M:%S',
      # stream=sys.stderr,
      filename='_'.join([args.output_prefix,'simulation',str(n)+'.log']),
      filemode=logmode
    )
    console = logging.StreamHandler()
    console.setLevel(logging.INFO)
    # set a format which is simpler for console use
    formatter = logging.Formatter('%(levelname)-5s @ %(asctime)s: %(message)s ','%a, %d %b %Y %H:%M:%S')
    #formatter.formatTime('%a, %d %b %Y %H:%M:%S')
    # tell the handler to use this format
    console.setFormatter(formatter)
    # add the handler to the root logger
    logging.getLogger('').addHandler(console)




def start_arena(n,args):
    shared_m=multiprocessing.Value(SharedMsg,-1,0.0,-1,"00","00",lock=True)
    miner_pool=[]
    for i in range(n):
        mi=Miner(i)
        #mi=multiprocessing.sharedctypes.RawValue(Miner,i)
        #mi.load_tsp_problem('tsp_N100_1.txt')
        mi.load_tsp_problem(args)
        miner_pool+=[mi]
    shared_arr=multiprocessing.Array('i',miner_pool[0].TSPInst.NCities*2)

    pool_jobs=[]

    for i in range(n):
      j=multiprocessing.Process(target=thread_p_func, name='Thread '+str(i),args=(miner_pool[i],shared_m,shared_arr))
      pool_jobs.append(j)
      j.start()
      logging.info(j.name+' started.')

    for jj in pool_jobs:
      jj.join()
      logging.info(jj.name+' completed.')

    # post processing
    logging.info('All threads completed.')

if __name__ == "__main__":
    args=parse_args()
    nplayer=args.num_players
    setup_logging(nplayer,args)
    logging.info('Start simulation:')
    start_arena(nplayer,args)
    
