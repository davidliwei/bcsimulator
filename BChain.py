import re
import sys
import random
import numpy as np
import itertools
#import matplotlib
#import matplotlib.pyplot as plt
import numpy
import time
import logging

#import TravelSalesman
#from TravelSalesman import TSPClass
import TSPClass
from ThreadCommu import *




class Block:
    block_index=0
    data_md5=''
    best_route=None
    best_route_value=None # value associated with best route
    timestamp=0
    difficulty=(0,0) # difficulty of searching
    def block_to_positions(self,N,K):
        rt=[]
        return random.sample([x for x in range(1,N)],K)
        # a completely random position; not for validation purpose
        #while(len(rt)<K):
        #    sp=random.randint(1,N-1)
        #    if sp in rt:
        #        pass
        #    else:
        #        rt+=[sp]
        #return rt






class Miner:
    miner_id=''
    block_queue=[] # should be a list of blocks
    TSPInst=None # should be a TSPInst object
    # record the winning information
    Winning_Route=None
    Winning_Info=None
    most_recent_trial=-1
    winning_msg=None

    neighbor_miners=[] # should be a list of Miner
    shared_message=None # an object consisting of SharedMsg object and a lock
    shared_array=None # an array to store route

    report_file_name=''

    def __init__(self,identifier):
        #self.miner_id=str(random.randint(0,1000000))
        self.miner_id=str(identifier)
        self.TSPInst=TSPClass.TSPClass(100)
        logging.info('Miner '+(self.miner_id)+' generated.')

    def setup_tsp_problem(self,cx,cy):
        self.TSPInst.setup_coordinates(cx,cy)
        self.report_file_name=self.miner_id+ '_N'+str(self.TSPInst.NCities) + '.block.txt'

    def load_tsp_problem(self,args):
        self.TSPInst.load_dist_file(args.tsp_file)
        self.report_file_name=args.output_prefix+'_'+self.miner_id+ '_N'+str(self.TSPInst.NCities) + '.block.txt'
        ofl=open(self.report_file_name,'w')
        print('\t'.join(['index','time','difficulty_N','difficulty_K','best_route_value','best_route']),file=ofl)
        ofl.close()

    def add_neighbor(self,nb):
        self.neighbor_miners+=[nb]



    '''
    Generate a random number as a simulation
    '''
    def random_search_number(self,current_trial,target,current_round=-1,shared_msg=None):
        # start the current trial of search
        rtval=1.0
        while True:
            rtval=random.random()
            time.sleep(random.random()*1)
            break_current_loop=False
            successful_notify=False
            if rtval <target:
                # found a solution
                #logging.info('M'+(self.miner_id)+': hit '+str(rtval)+' at r'+strcurrent_trial+'.')
                ## how to notify others about the solution?
                break_current_loop=True
            ##if self.if_others_found_solution(current_trial):
            if shared_msg!=None and current_round!=-1:
                if current_round <= shared_msg.round:
                    # break the current search, and return
                    rtval=1.0
                    break_current_loop=True
                # others found a solution
                #logging.info('--M'+self.miner_id+': others found a hit '+str(self.Winning_Route)+', stop r'+str(current_trial))
            # 
            # if we found a solution, either from us or from others
            if break_current_loop:
                break
            # post processing
            #    if current_trial<self.most_recent_trial:
            #        current_trial=self.most_recent_trial
            #    nb.block_index=current_trial
            #    nb.timestamp=time.time()
            #    nb.best_route=self.Winning_Route
            #    self.block_queue+=[nb]
        # end while 
        return rtval


    '''
    A pseudo simulation for mining
    parameters:
        shared_m
            A wrapped SharedMsg object
        shared_array
            A wrapped Array (integer) object
        use_random
            Whether to use random generator to simulate the process
    '''
    def pseudo_mining(self,shared_m,shared_arr,use_random=False):
        self.shared_message=shared_m
        self.shared_array=shared_arr
        max_n_trials=10000
        current_trial=0
        if use_random:
            target=0.05
        else:
            target=self.TSPInst.get_total_edge_weight()

        while current_trial < max_n_trials:
            # start the current trial of search
            nb=Block()
            # set up parameters (NCi, K)
            rtval=1.0
            (NCi,K)=self.get_current_block_parameters(self.TSPInst.NCities-1)
            if len(self.block_queue)>0:
                lastK=self.block_queue[-1].difficulty[1]
                lastN=self.block_queue[-1].difficulty[0]
            else:
                lastK=K
                lastN=NCi

            # whether to use random number as a toy example, or use TSP cities as a real example
            best_route=[]
            if use_random:
                if True:
                    # adjust target
                    if K>lastK or NCi>lastN:
                        target=target*0.8
                    elif K<lastK or NCi<lastN:
                        target=target/1.2
                    if K!=lastK or NCi!=lastN:
                        logging.info('M'+(self.miner_id)+': new target='+str(target)+' at r'+str(current_trial)+'. K='+str(K))
                # search
                rtval=self.random_search_number(current_trial,target,current_round=current_trial,shared_msg=self.shared_message)
            else:
                targetpos=nb.block_to_positions(self.TSPInst.NCities,NCi)
                #random_route=[0]+random.sample(range(1,self.TSPInst.NCities),self.TSPInst.NCities-1)+[0]
                random_route=[0]+targetpos+ random.sample([x for x in range(1,self.TSPInst.NCities) if x not in targetpos], self.TSPInst.NCities-1-len(targetpos)) + [0]
                tg_pos=[x for x in range(1,len(targetpos)+1)]
                #logging.info('--M'+(self.miner_id)+': r'+str(current_trial) 
                #        +', K:'+str(K)+', targetpos:'+str(targetpos)+',randomrote:'+str(random_route))
                logging.info('--M'+(self.miner_id)+': r'+str(current_trial) 
                        +',N:'+str(NCi)+ ', K:'+str(K))
                best_route=self.TSPInst.greedy_search_min_dist(random_route,K,targetposition=tg_pos,current_round=current_trial,shared_msg=self.shared_message)
                logging.info('M'+(self.miner_id)+': r'+str(current_trial) 
                        +', best route:'+str('-'.join([str(t) for t in best_route])))
                if len(best_route)==0:
                    rtval=numpy.Inf
                else:
                    rtval=self.TSPInst.get_route_dist(best_route)
                    logging.info('M'+(self.miner_id)+': hit '+str(rtval)+' at r'+str(current_trial)+'.')

            successful_notify=False
            if rtval <target:
                # I found a solution
                logging.info('M'+(self.miner_id)+': hit '+str(rtval)+' at r'+str(current_trial)+'.')
                #self.most_recent_trial=self.shared_message.round
                # how to notify others about the solution?
                #br='-'.join([str(t) for t in best_route])
                successful_notify=self.notify_neighbors(current_trial,rtval,best_route)
                self.Winning_Info=[t for t in best_route]
                #self.miner_id=self.miner_id+'_x'
                if successful_notify:
                    self.Winning_Route=rtval
                    self.most_recent_trial=current_trial
                else:
                    #logging.info('--M'+self.miner_id+' found a hit but fails to notify others at r'+str(current_trial))
                    pass
            # for two cases, either others found a solution (rtval>=target), or we cannot notify others (others found a solution ahead of us)
            if rtval>=target or successful_notify==False:
                self.Winning_Route=self.shared_message.value
                self.most_recent_trial=self.shared_message.round
                #self.Winning_Info=self.shared_message.msg2
                self.Winning_Info=[]
                self.shared_array.acquire()
                for i in range(self.TSPInst.NCities+1):
                    self.Winning_Info+=[self.shared_array[i]]
                self.shared_array.release()
                logging.info('--M'+self.miner_id+': others found a hit '+str(self.Winning_Route)+', stop r'+str(current_trial))
            # 
            # if we found a solution, either from us or from others
            if current_trial<self.most_recent_trial:
                current_trial=self.most_recent_trial
            nb.block_index=current_trial
            nb.timestamp=time.time()
            nb.best_route_value=self.Winning_Route
            #nb.best_route=[self.Winning_Route] # a temp record
            nb.best_route=[t for t in self.Winning_Info]
            nb.difficulty=(NCi,K)
            self.block_queue+=[nb]
            # increase the current trial
            current_trial+=1
            if current_trial % 10 ==0:
                self.write_block_to_file(self.report_file_name,start=current_trial-10,end=current_trial)


    '''
    Check if others found a solution
    '''
    def if_others_found_solution(self,current_trial):
        #return self.TSPInst.stop_search !=0
        #self.shared_message.acquire()
        if self.shared_message.round>=current_trial:
            self.Winning_Route=self.shared_message.value
            self.most_recent_trial=self.shared_message.round
            #self.winning_msg=str(self.shared_message.msg)
            return True
        #self.shared_message.release()
        return False

    '''
    Based on current block queue's parameters, calcuate the parameters needed for search
    '''
    def get_current_block_parameters(self,maxn):
        # default value
        XN=9
        XK=5
        # number of blocks used for calculateion
        nblock_used=7
        desired_time=30 # desired time, calculated in seconds
        if len(self.block_queue)<10:
            return (XN,XK)
        else:
            # use the last several blocks to calculate the time
            time_p=[self.block_queue[i].timestamp-self.block_queue[i-1].timestamp for i in range(len(self.block_queue)-nblock_used,len(self.block_queue))]
            average_time=sum(time_p)/len(time_p)
            frac=average_time/desired_time
            (last_N,last_K)=self.block_queue[-1].difficulty
            #print('time_p:'+str(self.block_queue[-1].timestamp)+'-'+str(self.block_queue[-2].timestamp))
            if frac<0.5:
                # need to increase 
                last_N=last_N+1
                #last_K=last_K+1
                if last_K>last_N:
                    last_K=last_N
            if frac>2:
                last_N=last_N-1
                #last_K=last_K-1
                if last_K<XK:
                    last_K=XK
            if last_N>maxn:
                last_N=maxn
            return (last_N,last_K)
            

    '''
    Notify neighbors
    Return True if succesfully updated
    '''
    def notify_neighbors(self,current_trial,rtval,best_route):
        if self.shared_message.round<current_trial:
            self.shared_message.acquire()
            self.shared_message.round=current_trial
            self.shared_message.value=rtval
            self.shared_message.msg=self.miner_id
            self.shared_message.release()
            self.shared_array.acquire()
            for i in range(len(best_route)):
                self.shared_array[i]=best_route[i]
            self.shared_array.release()
            return True
        else:
            logging.info('-- Current M' +self.miner_id +' is behind ('+str(current_trial)+'). Latest r'+str(self.shared_message.round))
            return False

    '''
    Write to file
    '''
    def write_block_to_file(self,filename,start=0,end=-1):
        logging.info(self.miner_id+' wrote block '+str(start)+' - '+str(end)+' to file...')
        if end==-1:
            end=len(self.block_queue)
        olfd=open(filename,'a')
        for ni in range(start,end):
            tbk=self.block_queue[ni]
            report_vec=[tbk.block_index,tbk.timestamp,tbk.difficulty[0],tbk.difficulty[1],tbk.best_route_value,'-'.join([str(t) for t in tbk.best_route])]
            print('\t'.join([str(t) for t in report_vec]),file=olfd)
        olfd.close()





    
