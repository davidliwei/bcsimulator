
from multiprocessing import Process, Lock
#from multiprocessing.sharedctypes import Value, Array
from ctypes import Structure, c_double, c_char_p, c_wchar_p, c_int


class SharedMsg(Structure):
    _fields_=[('round',c_int),('value',c_double),('winner',c_int), ('msg',c_wchar_p), ('msg2',c_wchar_p)]
