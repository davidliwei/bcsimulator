#!/usr/bin/env python

import sys
import random
from BChain import *
import multiprocessing
import logging
import multiprocessing.sharedctypes
import argparse


#from multiprocessing import Process, Lock
#from multiprocessing.sharedctypes import Value, Array
#from ctypes import Structure, c_double,c_wchar_p, c_char_p, c_int

def parse_args():
      parser=argparse.ArgumentParser(description='Running TSP for optimal simulation.')
      parser.add_argument('-t','--tsp-file',required=True,help='TSP problem definition file')
      # parser.add_argument('-n','--num-players',type=int,default=3,help='Number of players, default 3')
      parser.add_argument('-o','--output-prefix',help='Output prefix',default='')

      args=parser.parse_args()
      return args


if __name__ == "__main__":
    args=parse_args()
    TSPinst=TSPClass.TSPClass(1)
    TSPinst.load_dist_file(args.tsp_file)
    best_loop=TSPinst.find_tsp()
    best_loop_dist=TSPinst.get_route_dist(best_loop)

    report_file_name=args.output_prefix+ '_N'+str(TSPinst.NCities) + '.optimal.txt'
    ofl=open(report_file_name,'w')
    print('\t'.join(['index','time','difficulty_N','difficulty_K','best_route_value','best_route']),file=ofl)
    print('\t'.join(['0','0','0','0',str(best_loop_dist),'-'.join([str(x) for x in best_loop])]),file=ofl)
    ofl.close()


