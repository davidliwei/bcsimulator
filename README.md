# SciCoin: A Crypto-Currency for Scientific Computing #

Wei Li

*Children's National Hospital, Washington, DC*

*George Washington University, Washington, DC*

## Abstract ##
Many types of cryptocurrencies are proposed nowadays, including BitCoin, LiteCoin, ect. 
There are several drawbacks in these cryptocurrencies. 
First, huge amount of electricity is consumed to generate seemingly useless knowledge in mining.
Second, the total number of possible coins is limited. Mining is therefore harder and harder, and the available coins may not meet the increasing needs of the market.

We propose a new cryptocurrency system, SciCoin, to address these issues. 
SciCoin uses knowledge from solving computationally challenging problems (e.g., protein 3D structure prediction, protein-small molecule interaction). 
Therefore, mining SciCoin also helps understanding the nature of biology and designing novel drugs to combat disease.
SciCoin also incorpoates three different types of coins with different values, depending on mining difficulties and the number of possible coins. 
Different types of coins are similar to the actual coins used in real currency (bronze, silver and gold coins), and is able to meet different needs of users. 

This is an implementation of the algorithm in this [arXiv preprint: Adapting Blockchain Technology for Scientific Computing](https://arxiv.org/abs/1804.08230)

## Introduction ##

## SciCoin Design ##

### Blockchain model ###
Similar to other cryptocurrencies, SciCoin uses a peer-to-peer network structure to distribute with no central authority. 
SciCoin includes three different types of blocks.

### Type 1: local minimum ###
SciCoin chooses one computationally challenging problem, and aims to minimize an objective function **f**.
Take protein 3D structure prediction for example, for a certain protein *g*, users need to find a structure *s(g)*,
such that the energy of *s(g)* is minimized: *min* **f(s(g))**.

Once a local minimum value *local_min* **f(g)** is found, users can broadcast it to the SciCoin network.
To accept it, other nodes of the network need to verify the following:

* g is a valid protein structure;
* *s(g)* is not found in the current blockchain;
* *f(s(g))* reaches a local minimum;
* this block must point to a *problem definition* block that initiates the calculation of *g* (see type 3 below).

The local minimum can validated by randomly perturbing *s(g)* and compare its energy with *f(s(g))*.
If there are many nodes in the network, it can be easily validated.

For problems like protein folding, there may be a large number of local minimums for each protein *g*, 
and there may be a large number of possible protein sequences. 
Therefore, the possible number of local minimums available in the network is huge.

### Type 2: global minimum ###
Users can also claim *s(g)* as a global minimum energy state, which is a more difficult task.
Therefore this type of SciCoin is more valuable than type 1 (local minimum).

The block claiming a global minimum *global_min* **f(g)** cannot be used directly as a coin,
since later on others may find a new global minimum with smaller energy state.
To address this issue, when each node receives a block claiming a global minimum, it needs to do check following, in addition to the conditions in local minimum:

* *f(s(g))* is smaller than any other nodes in the blockchain;
* no other blocks whose energy *f* can beat it in a certain time window (e.g., 7 days).

The time window will serve as a "challenge phrase" to allow others to challenge this global minimum state.
If others find a new global minimum, the current block will be replaced.
If no other challenges are accepted during the phrase, 
the window will be closed and no longer accept further challenges.


Note that even if the window is closed in some nodes, the corresponding block may be still replaced,
since a new valid challenge may appear at the very end of the challenge phrase.
Users need to wait for some extra time to allow the majority nodes in the network to close the time window.

Claiming the global minimum is much more difficult than local minimum, and that's the reason it's more valuable.

### Type 3: problem definition ###
Early users of SciCoin will gain a huge advantage, since the possible number of proteins is huge.
Therefore, they can easily calculate local minimum by choosing a new protein *g* not in the blockchain, 
and not too many users can challenge the global minimum.

To avoid this, SciCoin designs another type of block, "problem definition" block.
The block defines which protein sequnece to be optimized, and type 1/2 blocks must link to this block as a valid block.

To increase the cost of finding this type of block, the block includes a *random character* field.
Users need to fill out this field such that the Hash value of the entire block meets certain requirement 
(e.g., multiple zeros in the beginning of the Hash value).
This design will increase the difficulty of calculating a new protein structure.

This type of SciCoin is similar to other cryptocurrencies. Finding this block is also difficult.

# Conclusion #

# References #







