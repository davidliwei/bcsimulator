import re
import sys
import random
import numpy as np
import itertools
import matplotlib
import matplotlib.pyplot as plt
import numpy
import time

canvas_size=100
# simulating N cities, from 0 to NCities
class TSPClass:
    NCities=7
    city_x=[]
    city_y=[]
    city_dist=np.identity(0)

    stop_search=0 # marker to terminate search early
    
    # initialization
    def __init__(self,n):
        self.NCities=n
        city_x=np.array([random.random()*canvas_size for x in range(self.NCities)])
        city_y=np.array([random.random()*canvas_size for x in range(self.NCities)])
        self.city_x=city_x
        self.city_y=city_y
        self.setup_dist_mat()
        self.subset_traceback.clear()

    def setup_coordinates(self,cx,cy):
        self.NCities=len(cx)
        self.city_x=cx
        self.city_y=cy
        self.setup_dist_mat()

    def save_dist_file(self,filename):
        outw=open(filename,'w')
        for ni in range(self.NCities):
            print('\t'.join([str(self.city_x[ni]),str(self.city_y[ni])]),file=outw)
        outw.close()

    '''
    Loading distance from file
    '''
    def load_dist_file(self,filename):
        nct=0
        nline=0
        cx=[]
        cy=[]
        for lines in open(filename):
            field=lines.strip().split()
            nline+=1
            try:
                i_x=float(field[0])
                i_y=float(field[1])
                nct+=1
                cx+=[i_x]
                cy+=[i_y]
            except ValueError:
                print('Error in parsing line '+str(nline))
                continue
        print(str(nct)+' cities loaded.')
        self.setup_coordinates(np.array(cx),np.array(cy))



    '''
    generating distance matrix
    '''
    def setup_dist_mat(self):
        self.city_dist=np.identity(self.NCities)
        city_x=self.city_x
        city_y=self.city_y

        for i in range(self.NCities):
            sdict=np.sqrt((city_x-city_x[i])*(city_x-city_x[i])+(city_y-city_y[i])*(city_y-city_y[i]))
            self.city_dist[i,]=sdict
        
    """
    Plot cities
    """
    def plot(self):
        plt.plot(self.city_x, self.city_y, 'ro')
        plt.axis([0, canvas_size, 0, canvas_size])
        plt.title('N='+str(self.NCities))
        plt.xlabel('x')
        plt.ylabel('y')
        for i in range(self.NCities):
            plt.annotate(str(i),(self.city_x[i],self.city_y[i]))
        plt.show()
            
    """
    Plot paths
    """
    def plotpath(self,rt):
        plt.plot(self.city_x, self.city_y, 'ro',[self.city_x[s] for s in rt],[self.city_y[s] for s in rt],'-')
        plt.axis([0, canvas_size, 0, canvas_size])
        plt.title('N='+str(self.NCities))
        plt.xlabel('x')
        plt.ylabel('y')
        
        for i in range(self.NCities):
            plt.annotate(str(i),(self.city_x[i],self.city_y[i]))
        
        plt.show()
    
    """
    Held–Karp algorithm for calculating TSP
    For more details see https://en.wikipedia.org/wiki/Held%E2%80%93Karp_algorithm

    Number the cities 1, 2, . . . , N and assume we start at city 1, and the distance between city i and city j is dij. 
    Consider subsets S ⊆ {2, . . . , N} of cities and, for c ∈ S, 
    let D(S, c) be the minimum distance, starting at city 1, visiting all cities in S and finishing at city c.
    First phase: if S = {c}, then D(S, c) = d1,c. Otherwise: D(S, c) = minx∈S−c (D(S − c, x) + dx,c ).
    Second phase: the minimum distance for a complete tour of all cities is M = minc∈{2,...,N} (D({2, . . . , N}, c) + dc,1 )
    A tour n1 , . . ., nN is of minimum distance just when it satisfies M = D({2, . . . , N}, nN ) + dnN,1 .
    """
        
    # a dictionary to store the minimum distances and its traceback
    # key: (S,c)
    # value: (D(S,c), [x \in S such that the distance is minimum])
    subset_traceback={}



    """
    recursively calculate D(S,c)
    calculate the minimum distance of node 0 -> subset S -> node c
    """
    def dist_subset_cities(self,S,c,printinfo=False):
        stkey=getstkey(S,c)
        if stkey in self.subset_traceback:
            return self.subset_traceback[stkey][0]
        if len(S)==1:
            min_dist= self.city_dist[0,S[0]]
            self.subset_traceback[stkey]=(min_dist,S[0])
            if printinfo:
                print('Calculating '+str(stkey)+'  dist:'+str(min_dist)+', traceback:'+str(S[0]))
            return min_dist
        min_dist=np.inf
        min_trace_back=-1
        S_c=[t for t in S if t !=c]
        for x in S:
            if x==c:
                continue
            rec_dist=self.dist_subset_cities(S_c,x)
            dxc=self.city_dist[x,c]
            if rec_dist+dxc<min_dist:
                min_dist=rec_dist+dxc
                min_trace_back=x
        self.subset_traceback[stkey]=(min_dist,min_trace_back)
        if printinfo:
            print('Calculating '+str(stkey)+'  dist:'+str(min_dist)+', traceback:'+str(min_trace_back))
        return min_dist

    """
    Find the exhausive TSP
    """
    def find_tsp(self,printinfo=False):
        min_dist=np.inf
        min_trace_back=-1
        
        allS=[x for x in range(1,self.NCities)]
        for x in range(1,self.NCities):
            rec_dist=self.dist_subset_cities(allS,x,printinfo=printinfo)
            dxc=self.city_dist[0,x]
            if rec_dist+dxc<min_dist:
                min_dist=rec_dist+dxc
                min_trace_back=x
        #
        # 
        if printinfo:
            print('min distance:'+str(min_dist))

        # trace_back:
        current_node=min_trace_back
        node_vec=[current_node,0]
        ##print(str(current_node)+'-> 0')
        allS=[x for x in range(1,self.NCities) ]
        ##print('current set:'+str(allS))
        while len(allS)!=1:
            stkey=getstkey(allS,current_node)
            prev_node=self.subset_traceback[stkey][1]
            ##print(str(prev_node)+'->'+str(current_node))
            node_vec=[prev_node]+node_vec
            allS=[t for t in allS if t !=current_node]
            ##print('current set:'+str(allS))
            current_node=prev_node
        node_vec=[0]+node_vec
        print('->'.join([str(t) for t in node_vec]))
        # validate
        c_dist=0
        for i in range(len(node_vec)-1):
            c_dist=c_dist+self.city_dist[node_vec[i],node_vec[i+1]]
        if printinfo:
            print('min distance:'+str(c_dist))
        return node_vec


    '''
    determine if a loop route is a valid loop
    '''
    def is_valid_ct(self,rt):
        if rt[0]!=0 or rt[len(rt)-1]!=0:
            return False
        if sum(rt)!=self.NCities*(self.NCities-1)/2:
            return False
        return True

    '''
    calculate the distance of a string
    must begins from 0 and ends with 0
    rt: [0,2,3,4,1,5,0]
    '''
    def get_route_dist(self,rt):
        totalds=0
        for i in range(len(rt)-1):
            totalds+=self.city_dist[rt[i],rt[i+1]]
        return totalds

    def get_total_edge_weight(self):
        totalds=0
        for i in range(self.NCities):
            for j in range(i+1,self.NCities):
                totalds+=self.city_dist[i,j]
        return totalds

    '''
    randomly permute the order of a route, with maximum K positions
    '''
    def permute_dist(self,rt,K):
        positions=random.sample(range(1,self,NCities),K) # these are positions
        cities=[rt[i] for i in positions]
        cities2=random.sample(cities,K)
        # rt=[x for x in rt]
        for si in range(len(positions)):
            rt[positions[si]]=cities2[si]
        return rt

    '''
    a greedy algorithm to exhausively search the minimum route, until no shorter distance is found
    paramters:
        rt
            the proposed path, must begin with 0 and end with 0, e.g.,
            [0,1,2,3,...NCities-1,0]
        K
            the size of neighbor defined
        targetposition
            the position to do permutation; if none, use all possible positions, by default it's [1,2,...NCities-1] 
        printinfo
            whether to print information
        current_round
            The current round of search
        shared_msg
            A SharedMsg wrapped object to check whether to stop searching in the middle
    return value:
        rt
            the optimized path; empty if terminated early
    '''
    def greedy_search_min_dist(self,rt,K,targetposition=None,printinfo=False,current_round=-1,shared_msg=None):
        if targetposition == None:
            targetposition=[x for x in range(1,self.NCities)]
        hit_minimum=False

        while hit_minimum==False:
            current_dist=self.get_route_dist(rt)
            if printinfo:
                print('Route: '+str(rt)+', dist:'+str(current_dist))
            has_replaced=False
            for positions in itertools.combinations(targetposition,K):
                cities=[rt[i] for i in positions]
                #cities2=random.sample(cities,K)
                for cities2 in itertools.permutations(cities,K):
                    rt0=[x for x in rt]
                    for si in range(K):
                        rt0[positions[si]]=cities2[si]
                    update_dist=self.get_route_dist(rt0)
                    if update_dist<current_dist:
                        #print('update: '+str(rt0))
                        #print('positions: '+str(positions))
                        #print('cities:'+str(cities))
                        #print('cities2:'+str(cities2))
                        #print('rt0:'+str(rt0))
                        rt=[x for x in rt0]
                        has_replaced=True
                        break
                if has_replaced:
                    break # find a new one
                #if self.stop_search!=0:
                if shared_msg!=None and current_round!=-1:
                    if current_round <= shared_msg.round:
                        # break the current search, and return
                        return []
            if has_replaced==False:
                hit_minimum=True
        return rt


def getstkey(S,c):
    """
    get the string key value for c \in S
    This represents a path beginning from 0, iterate all elements in S and ends in c
    """
    #S=[t for t in S if t!=c]
    srs=sorted(S)
    strsc=','.join([str(x) for x in srs])+'-'+str(c)
    return strsc
        
